# HTML-Kurs

Zurück zum [Inhaltsverzeichnis](index.html).

## Anhang A - Arten von Elementen

### Einzel stehendes Element

Es gibt in HTML-Elemente, die nur für sich alleine vorkommen, z. B. das `<br/>`-Element:

    Das ist ein Satz.<br/>
    Vor und nach diesem Satz erfolgt auf jeden Fall ein Zeilenumbruch.<br/>

Ein einzeln stehendes Element ist so aufgebaut:

1. Spitze Klammer auf `<`
2. Name des Elements, z. B. `br`
3. Attribute (falls nötig)
4. Schrägstrich `/`
5. Spitze Klammer zu `>`

Zusammen ergibt das `<br/>`.

Man sagt auch "leeres Element" dazu, weil es keinen Inhalt hat.

### Element mit Inhalt

Die meisten HTML-Elemente stehen nicht für sich, sondern haben einen Inhalt.

Als Beispiel nehmen wir hier mal das Absatz-Element (`<p>`):

    <p>Das ist ein Absatz.</p>

Ein Element mit Inhalt kommt immer als Paar.

Das öffnende Element ist so aufgebaut:

1. Spitze Klammer auf `<`
2. Name des Elements, z. B. `p`
3. Attribute (falls nötig)
4. Spitze Klammer zu `>`

Das schließende Element ist so aufgebaut:

1. Spitze Klammer auf `<`
2. Schrägstrich `/`
3. Name des Elements, z. B. `p`
4. Spitze Klammer zu `>`

Der Inhalt eines solchen Elements kann (bis auf einige Ausnahmen) selbst auch wieder Elemente enthalten.

### Zusammenfassung

Ein Element ohne Inhalt hat den Schrägstrich auf der rechten Seite, vor der schließenden spitzen Klammer.

Ein Element mit Inhalt, kommt immer als Paar.
Das öffnende Element enthalt keinen Schrägstrich.
Das schließende Element hat den Schrägstrich auf der linken Seite, nach der öffnenden spitzen Klammer.

Weiter zu [Anhang B](anhangB.html).
