# HTML-Kurs

Zurück zum [Inhaltsverzeichnis](index.html).

## Anhang B - Elemente, die alt oder ungünstig sind

### Einleitung

HTML hat eine lange Reise hinter sich. Einige Element wurden hinzugefügt,
andere wurden für veraltet erklärt.

Ohne guten Grund sollten einige Elemente nicht mehr verwendet werden.

### Farben, Schriftarten, usw.

Als HTML erfunden wurde, gab es noch kein CSS und auch kein JavaScript.

Damit dennoch die Webseite nicht zu eintönig ist wurden einige Elemente zur
Verbesserung der Anzeige eingeführt.

Diese sind in modernem HTML zu meiden, hier eine Liste:

- Das `font`-Element für Schriftarten.
- Das `center`-Element zum Zentrieren von Text.
- Das `blink`-Element für blinkenden Text.
- Das `marquee`-Element für Lauftexte.
- Die Elemente `b` (fett), `i` (schräg), `s` (durchgestrichen) und `u` (unterstrichen) für die weitere Veränderung der Schrift.

Diese Aufgaben wurden vollständig von CSS übernommen.

Es gibt noch nachfolgende Elemente, die es mehr auf die Bedeutung als auf die Darstellung abgesehen haben.
Diese sind nach wie vor in Ordnung:

- Das `strong`-Element für wichtige Informationen / Warnungen.
- Das `em`-Element für die Hervorhebung.
- Das `del`-Element für nachträglich entfernten Text.
- Das `ins`-Element für nachträglich ergänzten Text.

### Unterseiten

In den frühen Tagen von HTML gab es eine einfache Methode um eine Seite innerhalb
einer anderen Seite anzuzeigen.

Diese Methode ist leider aus mehreren Gründen recht problematisch.

Das `frame`-Element sollte daher möglichst nicht mehr genutzt werden.

Es hat Probleme im Bezug auf Barrierefreiheit und Benutzbarkeit im allgemeinen.

### Tabellen zur Unterteilung der Webseite

Tabellen sind gut und sinnvoll um tabellarische Daten anzuzeigen.

Leider werden sie oft dafür mißbraucht um eine Webseite in Abschnitte aufzuteilen.

Dafür gibt es in modernem HTML vor allem das `div`-Element, das statt dessen zusammen mit CSS verwendet werden sollte.

Merke: `table` ist für tabellarische Daten sinnvoll, `div` für jegliche andere Form der logischen Aufteilung!

Weiter zu [Anhang C](anhangC.html).
