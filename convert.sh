#!/bin/sh
find . -name '*.md' -exec sh -c 'pandoc -s -t html5 {} -o `basename -s .md {}`.html' \;
