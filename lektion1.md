# HTML-Kurs

Zurück zum [Inhaltsverzeichnis](index.html).

## Lektion 1 - Werkzeuge

### Editor zum Bearbeiten

HTML kann mit einem normalen Texteditor bearbeitet werden.

Unter Windows reicht der mitgelieferte [Microsoft Editor](https://de.wikipedia.org/wiki/Microsoft_Editor) aus,
eine bessere Variante wäre z. B. [Notepad++](https://notepad-plus-plus.org/).

Eine [Liste von Texteditoren](https://de.wikipedia.org/wiki/Liste_von_Texteditoren) gibt es auf Wikipedia.

### Webbrowser zum Anzeigen

Jeder Webbrowser in einer aktuellen Version kann für diesen Kurs verwendet werden.

Der Autor verwendet zu diesem Zeitpunkt [Mozilla Firefox](https://www.mozilla.org/de/firefox/).

Eine [Liste von Webbrowsern](https://de.wikipedia.org/wiki/Liste_von_Webbrowsern) gibt es auf Wikipedia.

Weiter zu [Lektion 2](lektion2.html).

