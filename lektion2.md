# HTML-Kurs

Zurück zum [Inhaltsverzeichnis](index.html).

Zurück zu [Lektion 1](lektion1.html)

## Lektion 2 - Eine einfache HTML-Seite

Damit eine Textdatei aus HTML-Datei gilt muss diese mindestens folgenden Aufbau haben:

    <html>
    Ich bin eine sehr einfache HTML-Seite.
    </html>

Die Datei wird überlicherweise als `index.html` in ein Verzeichnis auf der Festplatte abgelegt.

![Mousepad](mousepad1.png "Mousepad")

![Mousepad speichern unter](mousepad2.png "Mousepad speichern unter")

Sie kann dann mit dem Webbrowser geöffnet werden.

![Dateiverwaltung](dateiverwaltung1.png "Dateiverwaltung")

![Mozilla Firefox](firefox1.png "Firefox")

Weiter zu [Lektion 3](lektion3.html).

