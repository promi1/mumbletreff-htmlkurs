# HTML-Kurs

Zurück zum [Inhaltsverzeichnis](index.html).

Zurück zu [Lektion 2](lektion2.html)

## Lektion 3 - Eine HTML-Seite nach dem Standard

Hier ist nochmal die Webseite aus Lektion 2:

    <html>
    Ich bin eine sehr einfache HTML-Seite.
    </html>

Diese Seite ist leider noch nicht komplett, hält sich nicht an den "HTML 5"-Standard.

Hier ist eine komplette Webseite nach diesem Standard:

    <!DOCTYPE html>
    <html>
    <head>
    <title>Meine HTML-Seite</title>
    </head>
    <body>
    Ich bin eine HTML-Seite nach dem "HTML 5"-Standard.
    </body>
    </html>

Gehen wir mal etwas genauer auf die einzelnen Bestandteile ein.

### Der DOCTYPE

Die Angabe `<!DOCTYPE html>` sagt dem Webbrowser, dass die Datei sich im
"HTML 5"-Format befindet.

Sie muss genau so als erstes in der HTML-Datei stehen.

### Das HTML-Element

Direkt nach dem DOCTYPE folgt ein Element, das von `<html>` bis `</html>` geht.

Nach dem abschließenden `</html>` muss die Datei zu Ende sein.

### Das Kopf-Element

Innerhalb des HTML-Elements kommt zuerst das Kopf-Element von `<head>` bis `</head>`.

Der Kopf kann sich aus unterschiedlichen Angaben zusammensetzen, muss aber mindestens den Titel der Webseite enthalten.

Der Titel wird in einem Element von `<title>` bis `</title>` angegeben.

### Das Körper-Element

Das Element, in dem in einer Webseite am meisten passiert, ist das Körper-Element von `<body>` bis `</body>`.

Hier befinden sich die eigentlichen Informationen, die die Webseite vermittelt.

Es kann so gut wie alles enthalten, wie z. B.:

- Überschriften
- Textabschnitte
- Links
- Aufzählungen und Listen
- Grafiken
- Videos
- Abgetrennte Bereiche (z. B. Menüs)
- uvw.

Weiter zu [Lektion 4](lektion4.html).

