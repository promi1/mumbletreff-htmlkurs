# HTML-Kurs

Zurück zum [Inhaltsverzeichnis](index.html).

Zurück zu [Lektion 3](lektion3.html)

## Lektion 4 - Überschriften und Absätze

Um die Seite besser zu strukturieren kann man Überschriften erstellen.

Außerdem lässt sich Text in zusammenhängende Absätze unterteilen.

Hier ein Beispiel:

    <!DOCTYPE html>
    <html>
    <head>
    <title>Meine HTML-Seite</title>
    </head>
    <body>

    <h1>Haupt-Überschrift</h1>

    <h2>Unterpunkt</h2>

    <h3>Erster Abschnitt</h3>
    <p>Das ist ein Absatz.</p>
    <p>Das ist der zweite Absatz.</p>
    <p>Das ist der dritte Absatz.</p>

    <h3>Zweiter Abschnitt</h3>
    <p>
      Dieser Absatz besteht aus mehreren Sätzen.<br/>
      Die Sätze werden nahe beieinander dargestellt.<br/>
    </p>
    <p>
      Dies ist ein anderer Absatz.<br/>
      Er ist vom vorigen Absatz weiter entfernt.<br/>
    </p>

    </body>
    </html>

Dazu die Ausgabe im Webbrowser:

![Firefox](firefox2.png "Firefox")

Gehen wir genauer auf die neuen Elemente ein:

### Die Überschriften-Elemente

Die Überschrift, die am größten dargestellt wird, wird mit dem `<h1>`-Element markiert.

Weitere Übeschriften, die dann immer kleiner werden, markiert man mit `<h2>`, `<h3>`, `<h4>` und `<h5>`.

Es gibt also 5 Stufen von Überschriften, die es erlauben Text besser aufzuteilen.

### Das Absatz-Element

Text, der zusammen in einem Block dargestellt werden soll, wird mit dem `<p>`-Element markiert.

Zwischen den `<p>`-Elementen wird vom Webbrowser ein größerer Abstand dargestellt.

### Das Zeilenumbruch-Element

Der Webbrowser versucht normalerweise Text immer in möglichst langen Zeilen anzuzeigen.

Zwischen Absätzen ist immer mindestens ein Zeilenumbruch.

Soll innerhalb eines Absatzes ein Zeilenumbruch sein, kann mit `<br/>` ein solcher erzwungen werden.

Weiter zu [Lektion 5](lektion5.html).

