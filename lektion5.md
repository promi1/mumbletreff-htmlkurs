# HTML-Kurs

Zurück zum [Inhaltsverzeichnis](index.html).

Zurück zu [Lektion 4](lektion4.html)

## Lektion 5 - Abschnitte

### Einleitung

Absätze und Überschriften sind gut, um Text aufzuteilen.
Es gibt noch weitere Elemente, um besondere Abschnite zu markieren.

Diese kann man z. B. verwenden um ein Menü vom Hauptbereich der Seite abzutrennen.
Auch eine Fußleiste oder eine Seite mit mehreren Blöcken nebeneinander lassen
sich so realisieren.

Der HTML-Standard sieht dafür zwei Element vor: `div` und `span`.

### Block-Abschnitt (div)

In einer anderen Lektion ging es schon um Absätze.
Das dafür verwendete `p`-Element versucht normalerweise die komplette Breite
der Webseite für sich in Anspruch zu nehmen.

HTML nennt solche Elemente Block-Elemente oder seit HTML 5 auch Fluss-Elemente.

Um mehrere Elemente zu einem größeren Abschnitt zusammen zu fassen gibt es das
`div`-Element.
Das `div`-Element bildet einen Block-Abschnitt. Es versucht also 
(wie das `p`-Element) soviel Platz wie möglich in der Breite auszufüllen.

### Inzeiliger Abschnitt (span)

Als Gegenbegriff zu Block verwendet man den englischen Begriff "inline". 
Auf Deutsch gesagt: Inzeilige (also inline) Elemente verändern nur durch ihre
bloße Verwendung die Darstellung des inneren Bereichs nicht!

Im Gegensatz zu `div` markiert ein `span` lediglich einen Abschnitt.

`span` ist also inzeilig.

### Anmerkung / weitergehendes Wissen

Beide Abschnitt-Elemente spielen im Zusammenhang mit CSS eine große Rolle.

CSS soll in diesem Kurs jedoch noch nicht betrachtet werden.

Es sei nur schon einmal folgendes gesagt:

CSS kann die Position, Breite, Höhe und anderes Verhalten von Elementen beeinflussen.
Damit werden erweiterte Anpassungen am der Darstellung von Elementen überhaupt erst möglich.

Weiter zu [Lektion 6](lektion6.html).

